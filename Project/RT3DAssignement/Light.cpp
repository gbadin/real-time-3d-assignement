#include "Light.h"
#include "DemoMain.h"

void Light::initLight(rt3d::lightStruct light,int lightNumber)
{
	if(lightNumber < 11 && lightNumber >= 0)
	{
		for(int i= 0; i<4; i++)
		{
			m_diffuse[lightNumber*4+i] = light.diffuse[i];
			m_specular[lightNumber*4+i] = light.specular[i];
			m_position[lightNumber*4+i] = light.position[i];
		}
		m_constantAttenuation[lightNumber] = light.constantAttenuation;
		m_linearAttenuation[lightNumber] = light.linearAttenuation;
		m_quadraticAttenuation[lightNumber] = light.quadraticAttenuation;
		if((lightNumber+1) >m_maxLight)
			m_maxLight = lightNumber +1;
	}
}

void Light::deleteLight(int lightNumber)
{
	if(lightNumber+1 < m_maxLight)
	{
		for(int i= 0; i<4; i++)
		{
			m_diffuse[lightNumber*4+i] = 0;
			m_specular[lightNumber*4+i] = 0;
			m_position[lightNumber*4+i] = 0;
		
		}
		m_constantAttenuation[lightNumber] = 0;
		m_linearAttenuation[lightNumber] = 0;
		m_quadraticAttenuation[lightNumber] = 0;
		if(m_maxLight == (lightNumber+1))
			m_maxLight--;
	}
}

void Light::setUniform(GLuint shaderProgram)
{
	ShaderManager::useShader(shaderProgram);

	int uniformIndex;
	float *positions;
	positions = new float[m_maxLight*4];
	for(int i = 0; i < m_maxLight;i++)
	{
		glm::vec4 tmpvec;
		tmpvec[0] = m_position[i*4];
		tmpvec[1] = m_position[i*4+1];
		tmpvec[2] = m_position[i*4+2];
		tmpvec[3] = m_position[i*4+3];
		tmpvec = DemoMain::getInstance().getViewMatrix() * tmpvec;
		positions[i*4] = tmpvec[0];
		positions[i*4+1] = tmpvec[1];
		positions[i*4+2] = tmpvec[2];
		positions[i*4+3] = tmpvec[3];
	}
	uniformIndex = glGetUniformLocation(shaderProgram, "lightPosition");
	glUniform4fv(uniformIndex, m_maxLight, positions);

	if(lastShaderUsed!= shaderProgram)
	{
		lastShaderUsed= shaderProgram;
		// pass in light data to shader
		uniformIndex = glGetUniformLocation(shaderProgram, "ambient");
		glUniform4fv(uniformIndex, 1, ambient);
		uniformIndex = glGetUniformLocation(shaderProgram, "diffuse");
		glUniform4fv(uniformIndex, m_maxLight, m_diffuse);
		uniformIndex = glGetUniformLocation(shaderProgram, "specular");
		glUniform4fv(uniformIndex, m_maxLight, m_specular);
		//pass additionnal parametters for distance attenuation
		uniformIndex = glGetUniformLocation(shaderProgram, "constantAttenuation");
		glUniform1fv(uniformIndex, m_maxLight,m_constantAttenuation);
		uniformIndex = glGetUniformLocation(shaderProgram, "linearAttenuation");
		glUniform1fv(uniformIndex, m_maxLight,m_linearAttenuation);
		uniformIndex = glGetUniformLocation(shaderProgram, "quadraticAttenuation");
		glUniform1fv(uniformIndex, m_maxLight,m_quadraticAttenuation);


	}

	uniformIndex = glGetUniformLocation(shaderProgram, "numLight");
	glUniform1i(uniformIndex, m_maxLight);
	
}

void Light::setUniform(GLuint shaderProgram, int lightNumber)
{
	if(lightNumber < m_maxLight)
	{
		ShaderManager::useShader(shaderProgram);
		int uniformIndex;
		if(lastShaderUsed!= shaderProgram)
		{
			lastShaderUsed= shaderProgram;
			// pass in light data to shader
			uniformIndex = glGetUniformLocation(shaderProgram, "light.ambient");
			glUniform4fv(uniformIndex, 1, ambient);
			uniformIndex = glGetUniformLocation(shaderProgram, "light.diffuse");
			glUniform4fv(uniformIndex, 1,&m_diffuse[lightNumber*4]);
			uniformIndex = glGetUniformLocation(shaderProgram, "light.specular");
			glUniform4fv(uniformIndex, 1, &m_specular[lightNumber*4]);
			//pass additionnal parametters for distance attenuation
			uniformIndex = glGetUniformLocation(shaderProgram, "constantAttenuation");
			glUniform1f(uniformIndex, m_constantAttenuation[lightNumber]);
			uniformIndex = glGetUniformLocation(shaderProgram, "linearAttenuation");
			glUniform1f(uniformIndex, m_linearAttenuation[lightNumber]);
			uniformIndex = glGetUniformLocation(shaderProgram, "quadraticAttenuation");
			glUniform1f(uniformIndex, m_quadraticAttenuation[lightNumber]);
		}
		glm::vec4 tmpvec;
		tmpvec[0] = m_position[lightNumber*4];
		tmpvec[1] = m_position[lightNumber*4+1];
		tmpvec[2] = m_position[lightNumber*4+2];
		tmpvec[3] = m_position[lightNumber*4+3];
		tmpvec = DemoMain::getInstance().getViewMatrix() * tmpvec;
		rt3d::setLightPos(shaderProgram, glm::value_ptr(tmpvec));
	}
}

void Light::setLightPosition(float *position, int lightNumber)
{
	if(lightNumber >= 0 && lightNumber < m_maxLight)
	{
		for(int i = 0; i< 3; i++)
		{
			m_position[lightNumber*4+i] = position[i];
		}
	}
}
