#include "DemoMain.h"

#define PI 3.141592f


using namespace std;

DemoMain *DemoMain::s_instance;

////////////////
//STATIC MEMBERS
////////////////

// Something went wrong - print error message and quit
void DemoMain::exitFatalError(char *message)
{
    cout << message << " " << endl;
    cout << SDL_GetError();
    SDL_Quit();
	cin.get();
    exit(1);
}

DemoMain &DemoMain::getInstance()
{
	if(s_instance == NULL)// if there is no current instance
		s_instance = new DemoMain();
	if(!s_instance->m_isSetupDone)//if the setup is not done
		s_instance->setupRC();
	return *s_instance;
}

void DemoMain::deleteInstance()
{
	if(s_instance != NULL)
	{
		delete s_instance;
		s_instance = NULL;
	}
}

/////////////////
//INSTANCE MEMBER
/////////////////

void DemoMain::setupRC()
{
	/* Initialize default output device */
	if (!BASS_Init(-1,44100,0,0,NULL))
		cout << "Can't initialize device";

    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // If you request a context not supported by your drivers,
    // no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
 
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on

    // Turn on x4 multisampling anti-aliasing (MSAA)
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	m_windowWidth = 800;
	m_windowHeight = 600;

    // Create 800x600 window
    m_window = SDL_CreateWindow("SDL/GLM/OpenGL Real Time 3D Demo",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
             m_windowWidth, m_windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if (!m_window) // Check window was created OK
        exitFatalError("Unable to create window");

    // Create opengl context and attach to window
    m_glContext = SDL_GL_CreateContext(m_window);
    // set swap buffers to sync with monitor's vertical refresh rate
    SDL_GL_SetSwapInterval(1);

	GLenum err = glewInit();
	// Required on Windows... init GLEW to access OpenGL beyond 1.1
	// remove on other platforms
	if (GLEW_OK != err)
	{	// glewInit failed, something is seriously wrong.
		cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	m_isSetupDone = true;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}

DemoMain::DemoMain()
{m_isSetupDone = false;}


DemoMain::~DemoMain()
{
	if(m_isSetupDone)
	{
		SDL_GL_DeleteContext(m_glContext);
		SDL_DestroyWindow(m_window);
		m_isSetupDone = false;
	}
	SDL_Quit();
}

void DemoMain::init()
{
	///////
	//MESH
	///////

	Mesh *fence = new Mesh();
	fence->loadFromObjFile("fence.obj");
	Mesh *cube = new Mesh();
	cube->loadFromObjFile("cube.obj");
	Mesh *sphere = new Mesh();
	sphere->loadFromObjFile("sphere.obj");

	/////////////
	//REAL OBJECT
	/////////////

	rt3d::materialStruct material ={
		{0.2f, 0.2f, 0.4f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.8f, 1.0f}, // diffuse
		{0.8f, 0.8f, 1.0f, 1.0f}, // specular
		2.0f};  // shininess

	BaseMeshRenderer tmpRenderer;//we need only one because we we set it into the vector, this copy the object
								//so we always use the same renderer to set all objects

	//set the obstacles (could be coded in files instead)

	//create fences for the top of the scene
	tmpRenderer.setMesh(fence);
	tmpRenderer.initShaders("colour.vert","colour.frag");
	//m_test.initShaders("multiplephong-tex.vert","multiplephong-tex.frag");
	//tmpRenderer.setTexture("black.bmp");
	tmpRenderer.setUniformVec4("colour",glm::vec4(0.1f,0.1f,0.1f,1.0f));
	tmpRenderer.setMaterial(material);
	tmpRenderer.setScale(glm::vec3(0.5f,0.5f,0.5f));
	tmpRenderer.setYRotation(-90);
	tmpRenderer.setPosition(glm::vec3(-20.0f,9.0f,-14.0f));

	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-20.0f,9.0f,-7.0f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-20.0f,9.0f,0.0f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setYRotation(0);
	tmpRenderer.setPosition(glm::vec3(-23.5f,9.0f,-17.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-30.5f,9.0f,-17.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-37.5f,9.0f,-17.5f));
	m_Obstacles.push_back(tmpRenderer);

	//set Buildings
	rt3d::materialStruct materialBuilding ={
		{0.2f, 0.2f, 0.4f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.8f, 1.0f}, // diffuse
		{0.8f, 0.8f, 1.0f, 1.0f}, // specular
		0.5f};  // shininess

	tmpRenderer.setMesh(cube);
	tmpRenderer.initShaders("phong-tex.vert","phong-tex.frag");
	//m_test.initShaders("multiplephong-tex.vert","multiplephong-tex.frag");
	tmpRenderer.deleteUniform("colour");
	tmpRenderer.setMaterial(materialBuilding);
	tmpRenderer.setScale(glm::vec3(20.0f,35.0f,20.0f));
	tmpRenderer.setTexture("buildingsimple1.bmp");
	tmpRenderer.setPosition(glm::vec3(30.0f,15.5f,27.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-30.0f,18.5f,32.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(45.0f,15.5f,12.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(25.0f,16.5f,-42.5f));
	m_Obstacles.push_back(tmpRenderer);

	tmpRenderer.setScale(glm::vec3(20.0f,40.0f,20.0f));
	tmpRenderer.setTexture("buildingsimple3.bmp");
	tmpRenderer.setPosition(glm::vec3(10.0f,17.0f,27.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-40.0f,24.0f,12.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(45.0f,17.0f,-7.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(5.0f,18.0f,-42.5f));
	m_Obstacles.push_back(tmpRenderer);

	tmpRenderer.setScale(glm::vec3(20.0f,45.0f,15.0f));
	tmpRenderer.setPosition(glm::vec3(-15.0f,16.5f,10.0f));
	m_Obstacles.push_back(tmpRenderer);

	tmpRenderer.setScale(glm::vec3(20.0f,30.0f,20.0f));
	tmpRenderer.setTexture("buildingsimple2.bmp");
	tmpRenderer.setPosition(glm::vec3(-10.0f,15.0f,32.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-50.0f,24.0f,-7.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(45.0f,14.0f,-27.5f));
	m_Obstacles.push_back(tmpRenderer);
	
	//create walls
	tmpRenderer.setScale(glm::vec3(10.0f,10.0f,5.0f));
	tmpRenderer.setTexture("wall_base.bmp");
	tmpRenderer.setPosition(glm::vec3(30.0f,-1.0f,-5.0f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(20.0f,-1.0f,-5.0f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setScale(glm::vec3(5.0f,10.0f,10.0f));
	tmpRenderer.setPosition(glm::vec3(12.5f,-1.0f,-2.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(12.5f,-1.0f,12.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-7.5f,-1.0f,-2.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-7.5f,-1.0f,-12.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-7.5f,-1.0f,-22.5f));
	m_Obstacles.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-7.5f,-1.0f,-32.5f));
	m_Obstacles.push_back(tmpRenderer);

	//create the collectible spheres
	m_emissiveIntensity = 0.7f;
	m_increaseEmissiveFactor = -1;

	tmpRenderer.setMesh(sphere);
	tmpRenderer.initShaders("emissive.vert","emissive.frag");
	tmpRenderer.setUniform1f("emissive",m_emissiveIntensity);
	tmpRenderer.setScale(glm::vec3(0.5f,0.5f,0.5f));
	tmpRenderer.setTexture("fire.bmp");
	tmpRenderer.setPosition(glm::vec3(12.5f,-0.5f,5.0f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-2.5f,0.0f,-7.5f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(32.5f,0.0f,-10.0f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(12.5f,0.0f,-17.5f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-2.5f,1.5f,20.0f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-27.5f,7.5f,12.5f));
	m_sphere.push_back(tmpRenderer);
	tmpRenderer.setPosition(glm::vec3(-30.0f,10.0f,-10.0f));
	m_sphere.push_back(tmpRenderer);

	/////////
	//PLAYER
	/////////

	//create the player
	m_player.loadModel("pac3D.md2");
	m_player.initShaders("phong-tex.vert","phong-tex.frag");
	//m_player.initShaders("multiplephong-tex.vert","multiplephong-tex.frag");
	m_player.setTexture("pac3D.bmp");
	m_player.setCurrentAnimation(MD2_STAND);
	m_player.setMaterial(material);

	m_player.setScale(glm::vec3(0.03f,0.03f,0.03f));
	m_player.setPosition(glm::vec3(25.0F,-1.0f,15.0f));
	
	m_projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,100.0f);
	m_horizontalAngle = m_verticalAngle = 0;
	m_horizontalAngle= -PI/2;

	/////////
	//SKY BOX
	/////////

	m_sky = SkyBox();
	m_sky.init("SkyBox\\bottom.bmp", "SkyBox\\top55.bmp", "SkyBox\\left55.bmp", "SkyBox\\front55.bmp", "SkyBox\\right55.bmp", "SkyBox\\back55.bmp");

	///////
	//LIGHT
	///////

	//start with no light
	rt3d::lightStruct light ={
		{0.0f,0.0f,0.0f,1.0f},//diffuse
		{0.0f,0.0f,0.0f,1.0f},//specular
		{0.0f,0.0f,0.0f,1.0f},//position
		1.0f,//constant atenuation
		0.0f,//linear Attenuation
		0.0f//quadratic Attenuation
	};

	m_currentLightIntensity = 0;

	//set ambient for the scene
	m_light.ambient[0] = 0.0f;
	m_light.ambient[1] = 0.0f;
	m_light.ambient[2] = 0.0f;
	m_light.ambient[3] = 1.0f;
	
	//create quickly positions of the lights
	/*float positions[11][4] = {{0.0f,3.0f,3.0f,1.0f},
		{0.0f,2.0f,0.0f,1.0f},
		{1.0f,3.0f,3.0f,1.0f},
		{3.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
		{0.0f,3.0f,3.0f,1.0f},
	};

	//set 11 lights
	for(int i = 0; i < 3; i++)
	{
		//set current light position
		for(int j = 0; j< 4; j++)
		{
			light.position[j] = positions[i][j];
		}
		m_light.initLight(light,i);
	}*/

	m_light.initLight(light,0);

	/*float ** height = new float *[3];
	for(int i = 0; i < 3; i++)
	{
		height[i] = new float[3];
		height[i][0] = -2.0f;
		height[i][1] = 2.0f;
		height[i][2] = 0.0f;
	}*/

	////////
	//FLOOR
	////////

	m_floor.readFromFile("Floor.height");
	BaseMeshRenderer *floorRenderer = m_floor.getFloorRendrer();
	floorRenderer->detectCollision = false;
	floorRenderer->setMaterial(material);
	floorRenderer->setTexture("grndroad1mid.bmp");
	floorRenderer->setPosition(glm::vec3(0.0f,-1.0f,0.0f));
	floorRenderer->initShaders("phong-tex.vert","phong-tex.frag");
	//floorRenderer->initShaders("multiplephong-tex.vert","multiplephong-tex.frag");

	////////
	//SOUND
	////////

	Sound tmpSound;
	tmpSound.LoadFromFile("Sounds\\Supergiant Games - Bastion Original Soundtrack - 10 Mine, Windbag, Mine.mp3",BASS_SAMPLE_LOOP);
	tmpSound.changeVolume(35);
	m_sounds.push_back(tmpSound);
	tmpSound.LoadFromFile("Sounds\\Supergiant Games - Bastion Original Soundtrack - 02 A Proper Story.mp3",BASS_SAMPLE_LOOP);
	tmpSound.changeVolume(35);
	m_sounds.push_back(tmpSound);
	tmpSound.LoadFromFile("Sounds\\75712__timbre__remix-of-19588-freed-bel-e01-tone-magic-wand-2.wav",BASS_SAMPLE_OVER_POS);
	tmpSound.changeVolume(50);
	m_sounds.push_back(tmpSound);
	
	m_sounds[0].start();
	SDL_ShowCursor(SDL_DISABLE);
}

void DemoMain::draw()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClearColor(0.5, 0.5, 0.5, 1.0); // set background colour
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	m_sky.draw();

	m_light.setUniform(m_floor.getFloorRendrer()->getShader(),0);
	m_floor.draw();

	vector<BaseMeshRenderer>::iterator it;
	for(it = m_Obstacles.begin(); it < m_Obstacles.end(); it++)
	{
		m_light.setUniform(it->getShader(),0);
		it->draw();
	}
	for(it = m_sphere.begin(); it < m_sphere.end(); it++)
	{
		it->setUniform1f("emissive",m_emissiveIntensity);
		it->draw();
	}

	m_light.setUniform(m_player.getShader(),0);
	m_player.draw();

	SDL_GL_SwapWindow(m_window); // swap buffers
}

void DemoMain::update()
{
	//set the player position according to the height of the heightmap
	glm::vec3 playerPos = m_player.getPosition();
	playerPos.y = m_floor.getHeightOfCoordinate(playerPos.x,playerPos.z);
	m_player.setPosition(playerPos);

	////////////////////
	//KEY DOWN HANDELING
	////////////////////

	//get the key pressed
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_ESCAPE] )
		m_running = false;

	int forwardMove, sideMove;
	forwardMove = sideMove = 0;

	if ( keys[SDL_SCANCODE_A] )
		sideMove += -1;
	if ( keys[SDL_SCANCODE_D] )
		sideMove += 1;
	if ( keys[SDL_SCANCODE_W] )
		forwardMove += 1;
	if ( keys[SDL_SCANCODE_S] )
		forwardMove += -1;

	//////////////////////
	//CHOOSE THE ANIMATION
	//////////////////////

	if(sideMove || forwardMove)
		m_player.setCurrentAnimation(MD2_RUN);
	else
		m_player.setCurrentAnimation(MD2_STAND);

	////////////////////////
	//CALCULATION FOR CAMERA
	////////////////////////

	if(m_xMovementMouse != 0 || m_yMovementMouse != 0)//if anything change, we do some calculation to get the new angles
	{
		float mouseSpeed = 0.2f;
		m_horizontalAngle += mouseSpeed * m_timeElapsed * (float)(-m_xMovementMouse);
		m_verticalAngle   += mouseSpeed * m_timeElapsed * (float)(-m_yMovementMouse);
		//stop to prevent the flip of the camera, that will add some strange effect like left and right mouse movement inverted.
		if(m_verticalAngle > PI/2)
			m_verticalAngle = PI/2;
		if(m_verticalAngle < -PI/2)
			m_verticalAngle = -PI/2;
	}

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
	cos(m_verticalAngle) * sin(m_horizontalAngle),
	sin(m_verticalAngle),
	cos(m_verticalAngle) * cos(m_horizontalAngle)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(m_horizontalAngle - 3.14f/2.0f),
		0,
		cos(m_horizontalAngle - 3.14f/2.0f)
	);

	// Up vector : perpendicular to both direction and right
	glm::vec3 up = glm::cross( right, direction );

	if(sideMove != 0 || forwardMove!= 0)//if a key is pressed
	{
		float moveSpeed = 10.0;
		if(sideMove!= 0)
		{
			m_player.setPosition(m_player.getPosition() + right * (float)sideMove * m_timeElapsed * moveSpeed);
		}
		if(forwardMove!= 0)
		{
			glm::vec3 withoutY = direction;
			withoutY.y = 0;
			withoutY = glm::normalize(withoutY);
			m_player.setPosition(m_player.getPosition() + withoutY * (float)forwardMove * m_timeElapsed * moveSpeed);
		}
		//rotate the player and the light
		m_player.setYRotation(-90 + m_horizontalAngle/(2*PI)*360);
		glm::vec3 withoutY = direction;
		withoutY.y = 0;
		withoutY = glm::normalize(withoutY);
		glm::vec3 lightPosition =  m_player.getPosition()+ 0.6f*withoutY;
		m_light.setLightPosition(glm::value_ptr(lightPosition),0);
	}

	/////////////////////
	//COLLISION DETECTION
	/////////////////////
	handleCollisionDetection();	//done during the update of the camera beacause it can change the player position and
								//the movement of the player is done with the camera

	glm::vec3 yShift = glm::vec3(0.0f,0.5f,0.0f);

	//camera position : the player position, less 2.5 times the direction vector (2.5 is an arbitrary value) and
	//we put the camera a little higher with yShift to see better around the character
	glm::vec3 cameraPosition = m_player.getPosition() - glm::normalize(direction)*2.5f + yShift;

	if(m_xMovementMouse != 0 || m_yMovementMouse != 0 || sideMove != 0 || forwardMove!= 0)//if nothing have changed, we don't change the view matrix
		m_view = glm::lookAt(cameraPosition,// Camera is here
			m_player.getPosition() + yShift, // and looks here : at the same position, plus "direction"
			up);

	//reset the movement with the mouse
	m_xMovementMouse = 0;
	m_yMovementMouse = 0;

	if(m_emissiveIntensity > 0.7f || m_emissiveIntensity < 0.3f)
	{
		m_increaseEmissiveFactor*= -1;
	}
	m_emissiveIntensity += m_increaseEmissiveFactor*0.1f*m_timeElapsed;

	m_playerLastPosition = m_player.getPosition();
}

void DemoMain::handleCollisionDetection()
{
	m_player.calculateBoundingBox();
	vector<BaseMeshRenderer>::iterator it;
	bool CollisionAlreadyDone = false;
	for(it = m_Obstacles.begin(); it < m_Obstacles.end(); it++)
	{
		if(m_player.isCollision(*it))
		{
			glm::vec3 maxPointObject, maxPointPlayer, minPointObject, minPointPlayer;
			maxPointObject = it->getMaxPoint();
			minPointObject = it->getMinPoint();
			maxPointPlayer = m_player.getMaxPoint();
			minPointPlayer = m_player.getMinPoint();
			glm::vec3 newPosition = m_player.getPosition();
			if(m_playerLastPosition.x > maxPointObject.x)
			{
				newPosition.x = maxPointObject.x + m_player.getPosition().x - minPointPlayer.x;
			}
			else if(m_playerLastPosition.x < minPointObject.x)
			{
				newPosition.x = minPointObject.x + minPointPlayer.x - m_player.getPosition().x;
			}
			if(m_playerLastPosition.z > maxPointObject.z)
			{
				newPosition.z = maxPointObject.z + m_player.getPosition().z - minPointPlayer.z;
			}
			else if(m_playerLastPosition.z < minPointObject.z)
			{
				newPosition.z = minPointObject.z + minPointPlayer.z - m_player.getPosition().z;
			}

			m_player.setPosition(newPosition);
		}
	}

	vector<BaseMeshRenderer>::iterator toErase;
	bool isCollision = false;
	for(it = m_sphere.begin(); it < m_sphere.end(); it++)
	{
		if(m_player.isCollision(*it))
		{
			toErase = it;
			isCollision = true;
			incrementLight();
			m_sounds[2].start();
		}
	}
	if(isCollision)
		m_sphere.erase(toErase);
}

void DemoMain::incrementLight()
{
	m_currentLightIntensity++;

	rt3d::lightStruct light ={
		{0.6f,0.4f,0.6f,1.0f},//diffuse
		{0.9f,0.9f,0.6f,1.0f},//specular
		{0.0f,3.0f,3.0f,1.0f},//position
		0.2f,//constant atenuation
		0.4f,//linear Attenuation
		0.13f//quadratic Attenuation
	};

	switch(m_currentLightIntensity)
	{
		case 1:
			m_light.ambient[0] = 0.05f;
			m_light.ambient[1] = 0.05f;
			m_light.ambient[2] = 0.05f;
			break;
		case 2:
			m_light.ambient[0] = 0.1f;
			m_light.ambient[1] = 0.1f;
			m_light.ambient[2] = 0.1f;
			light.constantAttenuation = 0.18f;
			light.linearAttenuation = 0.3f;
			light.quadraticAttenuation = 0.1f;
			break;
		case 3:
			m_light.ambient[0] = 0.2f;
			m_light.ambient[1] = 0.2f;
			m_light.ambient[2] = 0.2f;
			light.constantAttenuation = 0.16f;
			light.linearAttenuation = 0.2f;
			light.quadraticAttenuation = 0.07f;
			break;
		case 4:
			light.constantAttenuation = 0.14f;
			light.linearAttenuation = 0.13f;
			light.quadraticAttenuation = 0.05f;
			break;
		case 5:
			light.constantAttenuation = 0.12f;
			light.linearAttenuation = 0.09f;
			light.quadraticAttenuation = 0.03f;
			break;
		case 6:
			light.constantAttenuation = 0.10f;
			light.linearAttenuation = 0.065f;
			light.quadraticAttenuation = 0.01f;
			break;
		case 7:
			light.constantAttenuation = 0.10f;
			light.linearAttenuation = 0.065f;
			light.quadraticAttenuation = 0.01f;
			m_light.ambient[0] = 0.4f;
			m_light.ambient[1] = 0.4f;
			m_light.ambient[2] = 0.4f;
			m_sounds[0].stop();
			m_sounds[1].start();
			break;
	}
	m_light.initLight(light, 0);
}

void DemoMain::handleEvents()
{
	SDL_Event sdlEvent;	// variable to detect SDL events

	while(SDL_PollEvent(&sdlEvent))
	{
		if (sdlEvent.type == SDL_QUIT)
			m_running = false;
		else if (sdlEvent.type == SDL_MOUSEMOTION)
		{
			if(sdlEvent.motion.x != m_windowWidth/2 || sdlEvent.motion.y != m_windowHeight/2)
			{
				m_xMovementMouse = sdlEvent.motion.xrel;
				m_yMovementMouse = sdlEvent.motion.yrel;
				SDL_WarpMouseInWindow(m_window,m_windowWidth/2,m_windowHeight/2);
			}
		}
	}
}

void DemoMain::run()
{
	clock_t currentTime, LastTime;
	currentTime = LastTime = clock();
	m_running = true;
	while (m_running)
	{
		currentTime = clock();
		m_timeElapsed = ((float)(currentTime - LastTime)/CLOCKS_PER_SEC);
		LastTime = currentTime;
		handleEvents();
		update();
		draw();
	}
}

float DemoMain::getElapsedTime()
{
	return m_timeElapsed;
}
