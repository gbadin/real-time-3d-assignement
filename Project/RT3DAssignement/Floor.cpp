#include "Floor.h"
#include <cmath>

Floor::~Floor()
{
	for(int i = 0; i< m_width; i ++)
	{
		delete [] m_heights[i];//delete each array in the array
	}
	delete [] m_heights;//delete the final array
}

float Floor::getHeightOfCoordinate(float x, float z)
{
	if(x > (m_totalWidth/2) - m_distanceBetweenPoints|| x < -m_totalWidth/2|| z > (m_totalLength/2) - m_distanceBetweenPoints|| z < -m_totalLength/2)
		return 0.0f;
	int nearestXInf, nearestYInf;
	float interpolation1, interpolation2, xRatio, zRatio;
	nearestXInf = (int)((x + m_totalWidth/2)/m_distanceBetweenPoints);
	nearestYInf = (int)((z + m_totalLength/2)/m_distanceBetweenPoints);
	xRatio = (x + m_totalWidth/2.0f - nearestXInf*m_distanceBetweenPoints)/m_distanceBetweenPoints;
	zRatio = (z + m_totalLength/2.0f - nearestYInf*m_distanceBetweenPoints)/m_distanceBetweenPoints;
	interpolation1 = m_heights[nearestXInf][nearestYInf]*(1-xRatio) + m_heights[nearestXInf+1][nearestYInf]*xRatio;
	interpolation2 = m_heights[nearestXInf][nearestYInf+1]*(1-xRatio) + m_heights[nearestXInf+1][nearestYInf+1]*xRatio;
	return interpolation1*(1-zRatio) + interpolation2*zRatio;
}

void Floor::calculateFloorFromheightMap(float **heights, int width, int length,float distanceBetweenPoints, float *textureCoordinates)
{
	m_distanceBetweenPoints = distanceBetweenPoints;
	m_heights = heights;
	m_width = width;
	m_length = length;

	m_totalLength = distanceBetweenPoints * m_length;
	m_totalWidth = distanceBetweenPoints * m_width;

	float xStart, yStart;
	xStart = -(m_totalWidth/2);
	yStart = -(m_totalLength/2);

	int numberOfFloat = (m_length-1)*(m_width-1);//number of square in the heightmap
	numberOfFloat *= 18;//there is 2 triangles per square 3 point per triangle and 3 floats per point. so 2*3*3 = 18
	GLfloat *vertices = new GLfloat[numberOfFloat]();
	GLfloat *normal = new GLfloat[numberOfFloat]();
	GLfloat *texCoord;
	if(textureCoordinates != nullptr)
	{
		texCoord = textureCoordinates;
	}
	else
	{
		texCoord = new GLfloat[numberOfFloat*2/3]();
	}
	

	//there is two triangle in each square
	for(int j = 0; j < m_length -1; j++)
	{
		for(int i = 0; i < m_width -1; i++)
		{
			int index = (i + j*(m_width-1))*18;//first index of the current line, each square have 18 float (2*3*3)
			//first point of the firts triangle
			vertices[index] = xStart+i*m_distanceBetweenPoints;//x
			vertices[index + 1] = heights[i][j];//y
			vertices[index + 2] = yStart+j*m_distanceBetweenPoints;//z
			//second point of the firts triangle
			vertices[index + 3] = xStart+i*m_distanceBetweenPoints;//x
			vertices[index + 4] = heights[i][j+1];//y
			vertices[index + 5] = yStart+(j+1)*m_distanceBetweenPoints;//z
			//third point of the firts triangle
			vertices[index + 6] = xStart+(i+1)*m_distanceBetweenPoints;//x
			vertices[index + 7] = heights[i+1][j];//y
			vertices[index + 8] = yStart+j*m_distanceBetweenPoints;//z
			

			/*
			for a triangle p1, p2, p3, if the vector U = p2 - p1 and the vector V = p3 - p1 then the normal N = U X V and can be calculated by:
			Nx = UyVz - UzVy
			Ny = UzVx - UxVz
			Nz = UxVy - UyVx
			*/
			//calculate normal of the triangle
			glm::vec3 norm;
			norm.x = (vertices[index + 4]- vertices[index + 1])*(vertices[index + 8]- vertices[index + 2]) - (vertices[index + 5] - vertices[index + 2])*(vertices[index + 7]- vertices[index + 1]);
			norm.y = (vertices[index + 5] - vertices[index + 2])*(vertices[index + 6] - vertices[index]) - (vertices[index + 3] - vertices[index])*(vertices[index + 8]- vertices[index + 2]);
			norm.z = (vertices[index + 3] - vertices[index])*(vertices[index + 7]- vertices[index + 1]) - (vertices[index + 4]- vertices[index + 1])*(vertices[index + 6] - vertices[index]);

			//first point of the second triangle
			vertices[index + 9] = xStart+(i+1)*m_distanceBetweenPoints;//x
			vertices[index + 10] = heights[i+1][j];//y
			vertices[index + 11] = yStart+j*m_distanceBetweenPoints;//z
			//second point of the second triangle
			vertices[index + 12] = xStart+i*m_distanceBetweenPoints;//x
			vertices[index + 13] = heights[i][j+1];//y
			vertices[index + 14] = yStart+(j+1)*m_distanceBetweenPoints;//z
			//third point of the second triangle
			vertices[index + 15] = xStart+(i+1)*m_distanceBetweenPoints;//x
			vertices[index + 16] = heights[i+1][j+1];//y
			vertices[index + 17] = yStart+(j+1)*m_distanceBetweenPoints;//z
			

			//calculate the normal of the second triangle
			glm::vec3 norm2;
			norm2.x = (vertices[index + 13]- vertices[index + 10])*(vertices[index + 17]- vertices[index + 11]) - (vertices[index + 14] - vertices[index + 11])*(vertices[index + 16]- vertices[index + 10]);
			norm2.y = (vertices[index + 14] - vertices[index + 11])*(vertices[index + 15] - vertices[index + 9]) - (vertices[index + 12] - vertices[index + 9])*(vertices[index + 17]- vertices[index + 11]);
			norm2.z = (vertices[index + 12] - vertices[index + 9])*(vertices[index + 16]- vertices[index + 10]) - (vertices[index + 13]- vertices[index + 10])*(vertices[index + 15] - vertices[index +9]);
			
			norm = glm::normalize(norm);
			norm2 = glm::normalize(norm2);

			//put normals in the normal tab Could be improved by putting normals that are the average of all the normal of the point to have smoother angles
			for(int k = 0; k < 3; k++)
			{
				normal[index+k*3] = norm.x;
				normal[index+k*3 +1] = norm.y;
				normal[index+k*3 +2] = norm.z;

				normal[index+k*3+9] = norm2.x;
				normal[index+k*3+10] = norm2.y;
				normal[index+k*3+11] = norm2.z;
			}

			//if no texture coordinates are given, we create new
			if(textureCoordinates == nullptr)
			{
				//firts triangle
				texCoord[index*2/3] = 0.0f;
				texCoord[index*2/3+1] = 1.0f;

				texCoord[index*2/3+3] = 0.0f;
				texCoord[index*2/3+4] = 1.0f;

				texCoord[index*2/3+5] = 1.0f;
				texCoord[index*2/3+6] = 1.0f;

				//second triangle
				texCoord[index*2/3+6] = 1.0f;
				texCoord[index*2/3+7] = 1.0f;

				texCoord[index*2/3+8] = 0.0f;
				texCoord[index*2/3+9] = 0.0f;

				texCoord[index*2/3+10] = 1.0f;
				texCoord[index*2/3+11] = 0.0f;
			}
		}
	}
	m_mesh.init(numberOfFloat/3,vertices,nullptr,normal,texCoord);
	m_floorRendrer.setMesh(&m_mesh);
}

void Floor::readFromFile(char* filename)
{
	GLint fileLength;
	char *fileSource = rt3d::loadFile(filename, fileLength);

	if (fileLength == 0)
	{
		std::cout << "file "<< filename <<" not found" << std::endl;
		return;
	}

	std::stringstream  fileStream;
	fileStream << fileSource;
	std::string line;
	float **heights;
	int width, length;
	float distanceBetweenPoints, *textureCoordinates;
	textureCoordinates = nullptr;
	int currentLine = 0, currentColumn = 0,currentTexCoord = 0;
	
	while (fileStream.good())
	{
		fileStream >> line;
		switch(line[0])
		{
			case 'd'://width, length and distance beetwen points
				fileStream >> width >> length >> distanceBetweenPoints;
				heights = new float*[width]();
				break;
			case 'h'://Current Height value
				if(currentLine == 0)//alocate memory if this in a new column
					heights[currentColumn] = new float[length];
				fileStream >> heights[currentColumn][currentLine];
				currentColumn ++;
				if(currentColumn == width)
				{
					currentColumn = 0;
					currentLine++;
				}
				break;
			case 't'://current Tecture Coordinate
				if(textureCoordinates == nullptr)
				{
					textureCoordinates = new float[(length-1)*(width-1)*2/3]();
				}
				fileStream >> textureCoordinates[currentTexCoord*2] >> textureCoordinates[currentTexCoord*2+1];
				currentTexCoord++;
				break;
			default:
				getline(fileStream, line);
				break;
		}
	}
	calculateFloorFromheightMap(heights,width,length,distanceBetweenPoints,textureCoordinates);
}

void Floor::draw()
{
	m_floorRendrer.draw();
}
