#ifndef DEMO_MAIN_H
#define DEMO_MAIN_H

#include "BaseMeshRenderer.h"
#include "AnimatedMeshRenderer.h"
#include "SkyBox.h"
#include "Light.h"
#include "Floor.h"
#include "bass.h"
#include "Sound.h"

#include <ctime>
#include <vector>

#include <SDL.h>

#include <GL/glew.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

class DemoMain
{
public :
	static DemoMain &getInstance();
	static void deleteInstance();

	void init();
	void draw();
	void update();
	void handleCollisionDetection();//function used to have a clearer code
	void run();
	void handleEvents();
	glm::mat4 getViewMatrix(){return m_view;}
	glm::mat4 getProjectionMatrix(){return m_projection;}
	void incrementLight();//change the light intensity (and also the sound when the last sphere is collected)

	float getElapsedTime();

	static void exitFatalError(char *message);

	Light* getLight(){return &m_light;}
	
private :
	static DemoMain *s_instance; //the instance of the singleton class
	SDL_GLContext m_glContext; // OpenGL context handle
    SDL_Window *m_window; // window handle
	bool m_isSetupDone;

	glm::vec3 m_playerLastPosition;

	void setupRC();
	
	DemoMain();
	~DemoMain();

	float m_timeElapsed;

	bool m_running;

	//size of the window
	int m_windowWidth, m_windowHeight;

	//used for the mouse motion
	int m_xMovementMouse, m_yMovementMouse;
	float m_horizontalAngle, m_verticalAngle;

	glm::mat4 m_view;
	glm::mat4 m_projection;

	std::vector<BaseMeshRenderer> m_Obstacles;
	std::vector<BaseMeshRenderer> m_sphere;
	AnimatedMeshRenderer m_player;

	SkyBox m_sky;

	Light m_light;

	float m_emissiveIntensity;
	int m_increaseEmissiveFactor;
	int m_currentLightIntensity;

	Floor m_floor;

	std::vector<Sound> m_sounds;
};

#endif
