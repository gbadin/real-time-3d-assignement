#include "Sound.h"
#include <iostream>

void Sound::LoadFromFile(char * filename, DWORD flag)
{
	if (m_sample=BASS_SampleLoad(FALSE,filename,0,0,3,flag))
		std::cout << "sample " << filename << " loaded!" << std::endl;
	else
	{
		std::cout << "Can't load sample";
	}
}

void Sound::stop()
{
	HCHANNEL ch=BASS_SampleGetChannel(m_sample,FALSE);
	if (!BASS_ChannelStop(ch))
			std::cout << "Can't stop channel, error N� :"<< BASS_ErrorGetCode() << std::endl;
	if (!BASS_SampleStop(m_sample))
			std::cout << "Can't stop sample, error N� :"<< BASS_ErrorGetCode() << std::endl;
}

void Sound::pause()
{
	HCHANNEL ch=BASS_SampleGetChannel(m_sample,FALSE);
	if (!BASS_ChannelPause(ch))
			std::cout << "Can't pause sample, error N� :"<< BASS_ErrorGetCode() << std::endl;
}

void Sound::resume()
{
	HCHANNEL ch=BASS_SampleGetChannel(m_sample,FALSE);
	if (!BASS_ChannelPlay(ch,FALSE))
			std::cout << "Can't resume sample, error N� :"<< BASS_ErrorGetCode() << std::endl;
}

void Sound::start()
{
	HCHANNEL ch=BASS_SampleGetChannel(m_sample,FALSE);
	if (!BASS_ChannelPlay(ch,TRUE))
			std::cout << "Can't play sample, error N� :"<< BASS_ErrorGetCode() << std::endl;
}

void Sound::changeVolume(int volume)
{
	HCHANNEL ch=BASS_SampleGetChannel(m_sample,FALSE);
	BASS_ChannelSetAttributes(ch,-1,volume,0);
}