// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330

uniform mat4 modelview;
uniform mat4 projection;

uniform vec4 lightPosition[11];//11 light because I was thinking to create 10 light and amore important light like the sun
uniform float constantAttenuation[11];//constant used for calculating the attenuation with distance
uniform float linearAttenuation[11];
uniform float quadraticAttenuation[11];

uniform int numLight;

in  vec3 in_Position;
in  vec3 in_Normal;
out vec3 ex_N;
out vec3 ex_V;
out vec3 ex_L[11];

out int ex_NumLight;

out float ex_att[11];

in vec2 in_TexCoord;
out vec2 ex_TexCoord;

// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {

	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;

	// surface normal in eye coordinates
	// taking the rotation part of the modelview matrix to generate the normal matrix
	// (if scaling is includes, should use transpose inverse modelview matrix!)
	// this is somewhat wasteful in compute time and should really be part of the cpu program,
	// giving an additional uniform input
	//mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	mat3 normalmatrix = mat3(modelview);
	ex_N = normalize(normalmatrix * in_Normal);

	// L - to light source from vertex
	for(int i = 0; i < min(11, ex_NumLight); i++)
	{
		ex_L[i] = normalize(lightPosition[i].xyz - vertexPosition.xyz);

		float distance = length(lightPosition[i].xyz - vertexPosition.xyz);

		ex_att[i] = 1/(0.1 + 0.08*distance + 0.02*distance*distance);
	}

	ex_TexCoord = in_TexCoord;

	ex_NumLight = numLight;

    gl_Position = projection * vertexPosition;

}