// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform vec4 ambient;
uniform vec4 diffuse[11];
uniform vec4 specular[11];

uniform materialStruct material;
uniform sampler2D textureUnit0;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L[11];

in float ex_att[11];

in int ex_NumLight;

in vec2 ex_TexCoord;

layout(location = 0) out vec4 out_Color;
 
void main(void) {
    
	vec4 totalLight;

	// Ambient intensity
	totalLight = ambient * material.ambient;

	for(int i = 0; i < min(11, ex_NumLight); i++)
	{
		// Diffuse intensity
		vec4 diffuseI = diffuse[i] * material.diffuse;
		diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L[i])),0);

		// Specular intensity
		// Calculate R - reflection of light
		vec3 R = normalize(reflect(normalize(-ex_L[i]),normalize(ex_N)));

		vec4 specularI = specular[i] * material.specular;
		specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);
		
		totalLight = totalLight + (specularI + diffuseI)*ex_att[i];
	}
	// Fragment colour
	float transparency = texture(textureUnit0, ex_TexCoord).a;
	vec4 colour = totalLight *texture(textureUnit0, ex_TexCoord);
	colour.a = transparency;
	out_Color = colour;
}