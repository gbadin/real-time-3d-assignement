#ifndef ANIMATED_MESH_RENDERER_H
#define ANIMATED_MESH_RENDERER_H

#include "abstractMeshRenderer.h"
#include "md2model.h"

class AnimatedMeshRenderer : public AbstractMeshRenderer
{
public:
	AnimatedMeshRenderer();
	~AnimatedMeshRenderer();
	void draw();
	void loadModel(char *filename);
	void setCurrentAnimation(int animation);

	void calculateBoundingBox();

	void calculateModelMatrix();
private:
	md2model m_animatedMesh;
	GLuint m_currentMeshID;
};

#endif