#ifndef SOUND_H
#define SOUND_H

#include "bass.h"

class Sound
{
public:
	void LoadFromFile(char * filename, DWORD flag);
	void stop();
	void pause();
	void resume();
	void start();
	void changeVolume(int volume);
private:
	HSAMPLE m_sample;
};

#endif