#ifndef LIGHT_H
#define LIGHT_H

#include "rt3d.h"
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>

class Light
{
public:
	Light(){m_maxLight = 0;}
	~Light(){}
	void initLight(rt3d::lightStruct light,int lightNumber);
	void deleteLight(int lightNumber);
	void setUniform(GLuint shaderProgram);
	void setUniform(GLuint shaderProgram, int lightNumber);
	void setLightPosition(float *position, int lightNumber);

	//parameters of the light stored in the struct
	float ambient[4];
private:
	GLuint lastShaderUsed;

	int m_maxLight;

	GLfloat m_diffuse[44];
	GLfloat m_specular[44];
	GLfloat m_position[44];
	GLfloat m_constantAttenuation[11];
	GLfloat m_linearAttenuation[11];
	GLfloat m_quadraticAttenuation[11];
};
#endif