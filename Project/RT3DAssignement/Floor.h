#ifndef FLOOR_H
#define FLOOR_H

#include "BaseMeshRenderer.h"

//note : I have done some mistakes in logic between width, column, row and length so it can be hard to understand how it's working. X and Z axis seems to be inverted

class Floor
{
public:
	Floor(){}
	~Floor();

	void calculateFloorFromheightMap(float **heights, int width, int length,float distanceBetweenPoints, float *textureCoordinates = nullptr);
	void readFromFile(char* filename);

	void draw();

	float getHeightOfCoordinate(float x, float z);

	float getTotalLength(){return m_totalLength;}
	float getTotalWidth(){return m_totalWidth;}
	float getDistanceBeetwenPoints(){return m_distanceBetweenPoints;}

	int getWidth(){return m_width;}
	int getLength(){return m_length;}

	float **getHeights(){return m_heights;}

	BaseMeshRenderer *getFloorRendrer(){return &m_floorRendrer;}

private:
	Mesh m_mesh;
	BaseMeshRenderer m_floorRendrer;
	float m_totalLength, m_totalWidth, m_distanceBetweenPoints;
	float ** m_heights;
	int m_width, m_length;
};

#endif